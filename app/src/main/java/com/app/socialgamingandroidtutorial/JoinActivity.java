package com.app.socialgamingandroidtutorial;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class JoinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        final Button JoinGameButton = findViewById(R.id.JoinToGame);
        final String[] selectedgameID = new String[1];
        ArrayList<String> GamesList=new ArrayList<String>();
        //ArrayList<ArrayList<String>> userList=new ArrayList<ArrayList<String>>();
        //ArrayList<String> nameArray = new ArrayList<String>();
        final ListView GamList;
        //final ListView UsrList;

        HttpGetter getGame = new HttpGetter();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission")
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        getGame.execute("game", ""+lastKnownLocation.getLongitude(),""+lastKnownLocation.getLatitude() , "getGamesAround");


        try{

                    String games = getGame.get();
                    if (!games.equals("{ }")) {
                        JSONObject gms = new JSONObject(games);
                        JSONArray gmsArray = gms.getJSONArray("GamesPlusParts");


                        for (int i = 0; i < gmsArray.length(); i++) {
                            JSONObject game = gmsArray.getJSONObject(i);
                            String gameId = game.getString("GameID");
                            GameObject.getInstance().setUid(gameId);
                            boolean started = game.getBoolean("started");
                            Log.d("INFORMATION", "" + gameId);
                            if(started){
                                GamesList.add(""+gameId);
                            }

                            JSONArray playernames = (JSONArray) game.get("names");

                            /*
                            for (int j=0; j < playernames.length(); j++) {
                                JSONObject nameObj = playernames.getJSONObject(j);
                                String name = nameObj.get("name").toString();
                                nameArray.add(name);

                            }
                            userList.add(nameArray);
                            */
                        }


                    } else System.out.println("No Games Found");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        GamList = findViewById(R.id.GamesList);
        ArrayAdapter gamAd = new ArrayAdapter(JoinActivity.this, android.R.layout.simple_list_item_1,GamesList);
        GamList.setAdapter(gamAd);

        /*
        UsrList = findViewById(R.id.UserList);
        ArrayAdapter userAd = new ArrayAdapter(JoinActivity.this, android.R.layout.simple_list_item_1,userList);
        UsrList.setAdapter(userAd);
        */
        GamList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = GamList.getItemAtPosition(position);
                Toast.makeText(getBaseContext(),position+": "+o.toString(),Toast.LENGTH_SHORT).show();
                String item = GamList.getAdapter().getItem(position).toString();

                String[] sp = item.split("GameId:");

                selectedgameID[0] = sp[0];

            }
        });

        JoinGameButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
               JoinGame(selectedgameID[0]);
            }
        });
    }

    private void JoinGame(String gameid) {
        HttpPoster pushPartIntoGame = new HttpPoster();
        pushPartIntoGame.execute("game", gameid, FirebaseAuth.getInstance().getUid() ,"pushPartIntoGame");
        new HttpPoster();
        Intent intent = new Intent(JoinActivity.this, wait_for_start_game.class);
        startActivity(intent);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(JoinActivity.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(JoinActivity.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
