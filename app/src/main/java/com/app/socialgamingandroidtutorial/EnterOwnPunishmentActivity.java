package com.app.socialgamingandroidtutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class EnterOwnPunishmentActivity extends AppCompatActivity {


    Spinner dropdown;
    EditText enterPunish;


    String[] items = new String[]{"Yourself","All"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_own_punishment);

        final Button start = findViewById(R.id.button2);
        enterPunish = findViewById(R.id.enterPunish);
        dropdown = findViewById(R.id.spinner1);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EnterOwnPunishmentActivity.this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);



        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enterPunish.getText().toString().equals(""))
                {
                    String uuid = GameObject.getInstance().getUuid();

                    HttpPoster post = new HttpPoster();

                    String punishment = null;
                    try {
                        punishment = URLEncoder.encode(enterPunish.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    int pos;

                    switch(dropdown.getSelectedItemPosition())
                    {
                        case 0:
                            pos = 0;
                            break;
                        case 1:
                            pos = 2;
                            break;
                        default:
                            pos = 0;
                            break;
                    }

                    post.execute("game", uuid,  punishment, ""+pos,"addPunishment");
                    new HttpPoster().execute("game", uuid, "start");
                    Intent intent = new Intent(EnterOwnPunishmentActivity.this, GameActivity.class);
                    startActivity(intent);
                }else{}
            }
        });
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(EnterOwnPunishmentActivity.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(EnterOwnPunishmentActivity.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
