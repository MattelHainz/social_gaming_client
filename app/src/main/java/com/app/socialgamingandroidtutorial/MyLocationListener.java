package com.app.socialgamingandroidtutorial;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.content.Context ;

import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/*---------- Listener class to get coordinates ------------- */
class MyLocationListener implements LocationListener {


    public void saveLocationDb(Location loc){

    }

    @Override
    public void onLocationChanged(Location loc) {

        LatLng location = new LatLng(loc.getLatitude(), loc.getLongitude());
        Log.d("Test", "Http POST");
        new HttpPoster().execute("position",
                FirebaseAuth.getInstance().getUid(),
                "" + location.longitude,
                "" + location.latitude,
                "update");
       //String longitude = "Longitude: " + loc.getLongitude();
       //String latitude = "Latitude: " + loc.getLatitude();


        //Log.d("Test",longitude);
        //Log.d("Test",latitude);
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}