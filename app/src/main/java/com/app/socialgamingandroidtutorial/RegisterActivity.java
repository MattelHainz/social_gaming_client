package com.app.socialgamingandroidtutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Simon on 27.02.2018.
 */

public class RegisterActivity extends Activity{

    //TAG for Logger messages
    private static final String TAG= "RegisterActivity";

    //FirebaseAuth object that is needed to interact with Firebase
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();


        //Gets the objects in the layout file
        final EditText emailText = (EditText) findViewById(R.id.emailTextRegister);
        final EditText passwordText = (EditText) findViewById(R.id.passwordTextRegister);
        final Button register = findViewById(R.id.registerButtonRegister);

        //adds an onClickListener to the register Button
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register(emailText.getText().toString(),passwordText.getText().toString());
            }
        });
    }


    //Registers the user if account does not exist
    private void register(String email, String password){
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "createdUserwithEmail:success");
                    Intent intent = new Intent(RegisterActivity.this, MapsActivity.class);
                    startActivity(intent);
                } else {
                    Log.d(TAG,"createdUserwithEmail:no success");
                    Toast.makeText(RegisterActivity.this, "Registration failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
