package com.app.socialgamingandroidtutorial;

public class GameObject {
    private static final GameObject ourInstance = new GameObject();

    public static GameObject getInstance() {
        return ourInstance;
    }

    private String uuid;

    private GameObject() {
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUid(String uuid)
    {
        this.uuid = uuid;
    }

}
