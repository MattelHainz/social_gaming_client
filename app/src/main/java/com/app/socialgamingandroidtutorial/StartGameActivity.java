package com.app.socialgamingandroidtutorial;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.util.Log;


import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.Object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class StartGameActivity extends AppCompatActivity {

    private LocationManager locationManager;
    Handler handler;
    Runnable runner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

        final Button startGame = findViewById(R.id.StartGame);
        final TextView PlayerList = findViewById(R.id.PlayerList);

        HttpPoster createGame = new HttpPoster();

        final HttpGetter getjoinedPlayer = new HttpGetter();


        final String id = UUID.randomUUID().toString();

        GameObject.getInstance().setUid(id);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission")
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        createGame.execute("game", id, FirebaseAuth.getInstance().getUid(), "" + lastKnownLocation.getLongitude(), "" + lastKnownLocation.getLatitude(), "createGame");

        //public static final long CHECK_DELAY = 500;

        handler = new Handler();
        final int delay = 5000; //milliseconds

        handler.postDelayed(runner = new Runnable(){
            public void run(){
                HttpGetter getjoinedPlayer = new HttpGetter();
                getjoinedPlayer.execute("game", id, "getJoined");
                try {
                    String joined = getjoinedPlayer.get();
                    Log.d("INFORMATION","IM HERE" + joined);
                    if(!joined.equals("{ }")){
                        JSONObject plyrs = new JSONObject(joined);
                        JSONArray plyrsArray = plyrs.getJSONArray("allJoined");
                        PlayerList.setText("");
                        for (int i = 0; i < plyrsArray.length(); i++) {
                            JSONObject plyr = plyrsArray.getJSONObject(i);
                            String name = plyr.getString("name");
                            PlayerList.append(name + "\n");
                        }
                    }else System.out.println("Empty");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                handler.postDelayed(this, delay);


            }
        }, delay);



        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(runner,this);
                new HttpPoster().execute("game", id, "close");
                Intent intent = new Intent(StartGameActivity.this, ChoosePunishmentActivity.class);
                startActivity(intent);
            }
        });



    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            handler.removeCallbacks(runner,this);
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        handler.removeCallbacks(runner,this);
        AlertDialog alertDialog = new AlertDialog.Builder(StartGameActivity.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(StartGameActivity.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
