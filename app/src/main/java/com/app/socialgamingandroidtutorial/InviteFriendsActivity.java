package com.app.socialgamingandroidtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class InviteFriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        HttpGetter getFriends = new HttpGetter();
        getFriends.execute("position", "getPositionOfFriends");
        try{
            String friends = getFriends.get();
            if (!friends.equals("{ }")) {
                JSONObject frnds = new JSONObject(friends);
                JSONArray frndsArray =  frnds.getJSONArray("AllFriends");

                for (int i = 0; i < frndsArray.length(); i++) {
                    JSONObject frnd = frndsArray.getJSONObject(i);
                    String name = frnd.getString("user");
                    TextView FriendsList = findViewById(R.id.FriendsList);
                    FriendsList.append(name + "\n");
                }
            }else System.out.println("Empty");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    /*public void onOkayClick (){
        Intent intent = new Intent(InviteFriendsActivity.this, StartGameActivity.class);
        startActivity(intent);
    }*/


}
