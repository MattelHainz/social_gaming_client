package com.app.socialgamingandroidtutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class wait_for_start_game extends AppCompatActivity {

    final String id = GameObject.getInstance().getUuid();
    Handler handler;
    Runnable runner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_for_start_game);
        handler = new Handler();
        final int delay = 5000; //milliseconds

        handler.postDelayed(runner = new Runnable() {
            public void run() {
                HttpGetter checkLeft = new HttpGetter();
                checkLeft.execute("game", id, "checkLeft");
                try {
                    String left = checkLeft.get();
                    Log.d("info",left);
                    if (!left.equals("{ }")) {
                        JSONObject game = new JSONObject(left);
                        boolean runs = game.getBoolean("running");
                        if (runs) {
                            handler.removeCallbacks(this);
                            Intent intent = new Intent(wait_for_start_game.this, GameActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            handler.postDelayed(this, delay);
                        }
                    } else System.out.println("Empty");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }, delay);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            handler.removeCallbacks(runner,this);
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        handler.removeCallbacks(runner,this);
        AlertDialog alertDialog = new AlertDialog.Builder(wait_for_start_game.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(wait_for_start_game.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
