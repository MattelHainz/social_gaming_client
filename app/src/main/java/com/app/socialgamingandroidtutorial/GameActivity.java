package com.app.socialgamingandroidtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.UUID;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;



//mostly taken from https://gist.github.com/steveliles/11116937
public class GameActivity extends AppCompatActivity{
    final String id = GameObject.getInstance().getUuid();
    Handler handler = new Handler();

    boolean first = true;
    boolean runs = true;
    boolean stop = false;


    int prts;
    int vts;
    float progf;
    int prog;


    ProgressBar progress;

    Button leave;
    Button vote;
    final int delay = 5000; //milliseconds

    public void finish()
    {
        handler.removeCallbacks(runner);
    }

    Runnable runner = new Runnable() {

        public void run() {
            HttpGetter getVotes = new HttpGetter();
            HttpGetter checkLeft = new HttpGetter();
            getVotes.execute("game", id, "getVotes");
            try {
                String votes = getVotes.get();
                if (!votes.equals("{ }")) {
                    JSONObject game = new JSONObject(votes);
                    JSONObject ga = game.getJSONObject("votes");
                    prts = ga.getInt("parts");
                    vts = ga.getInt("votecount");
                    progf = (float)vts / (float)prts * 100;
                    prog =(int )(progf);
                    Log.d("prog",""+prog);
                    progress.setProgress(prog);
                    if (prog == 100) {
                        stop = true;
                        finish();
                        new HttpPoster().execute("game", id, "finishVote");
                        Intent intent = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else if (stop != true)
                        handler.postDelayed(this, delay);


                } else System.out.println("Empty");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            checkLeft.execute("game", id, "checkLeft");
            try {
                String left = checkLeft.get();
                if (!left.equals("{ }")) {
                    JSONObject game = new JSONObject(left);
                    runs = game.getBoolean("running");
                    Log.d("RunningBoolean",""+runs);
                    if (runs == false) {
                        stop = true;
                        finish();
                        Intent intent = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else if (stop != true)
                        handler.postDelayed(this,delay);
                } else System.out.println("Empty");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        leave = findViewById(R.id.leaveGame);

        progress = findViewById(R.id.progressBar);
        progress.setMax(100);
        progress.setProgress(0);



        vote = findViewById(R.id.vote);

        if(first) {
            runner.run();
        }
        else
            first = false;

        vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpPoster().execute("game", id, "voteForFinish");
            }
        });

        leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stop = true;
                handler.removeCallbacks(runner);


                new HttpPoster().execute("game", id , "userLeft");


                HttpGetter getPunish = new HttpGetter();
                String pun = "";
                getPunish.execute("game", id , "getPunish");
                try {
                    String punish = getPunish.get();
                    if (!punish.equals("{ }")) {
                        JSONObject game = new JSONObject(punish);
                        JSONObject ga = game.getJSONObject("Strafe");
                        pun = ga.getString("parts");
                    } else System.out.println("Empty");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                HttpGetter getjoinedPlayer = new HttpGetter();
                getjoinedPlayer.execute("game", id, "getJoined");
                try {
                    String joined = getjoinedPlayer.get();
                    if(!joined.equals("{ }")){
                        JSONObject plyrs = new JSONObject(joined);
                        JSONArray plyrsArray = plyrs.getJSONArray("allJoined");
                        for (int i = 0; i < plyrsArray.length(); i++) {
                            JSONObject plyr = plyrsArray.getJSONObject(i);
                            String name = plyr.getString("name");
                            //if(!name.equals(FirebaseAuth.getInstance().getUid())){
                            new HttpPoster().execute("punish", FirebaseAuth.getInstance().getUid(), name, pun, "pushPunishIntoListG");
                            //}

                        }
                    }else System.out.println("Empty");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(GameActivity.this, MainActivity.class);
                startActivity(intent);
            }


        });

    }

}
