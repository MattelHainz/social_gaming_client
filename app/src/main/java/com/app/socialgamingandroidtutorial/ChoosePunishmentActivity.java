package com.app.socialgamingandroidtutorial;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ChoosePunishmentActivity extends AppCompatActivity {

    private LocationManager locationManager;
    public int pos;

    final String id = GameObject.getInstance().getUuid();

    ListView dynamic;
    TextView eventsList;
    ArrayList<String> strafen;
    ArrayList<Integer> flags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_punishment);


        HttpGetter getEvent = new HttpGetter();
        final Button startGame = findViewById(R.id.Start);
        final Button choose = findViewById(R.id.EnterOwnPunish);
        eventsList = findViewById(R.id.EventsList);
        dynamic = findViewById(R.id.dynamicChoose);
        eventsList.setText("");
        strafen = new ArrayList<String>();
        flags = new ArrayList<Integer>();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission")
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //TODO: methode für koordinaten über Handy?
        getEvent.execute("event", "" + lastKnownLocation.getLongitude(), "" + lastKnownLocation.getLatitude(), "getEvent");
        try{
            String events = getEvent.get();

            if (!events.equals("{ }")) {
                JSONObject vnts = new JSONObject(events);
                Log.d("info", vnts.toString());
                JSONArray vntsArray =  vnts.getJSONArray("EventsAround");

                for (int i = 0; i < vntsArray.length(); i++) {
                    JSONObject vnt = vntsArray.getJSONObject(i);
                    String strafe1 = vnt.getString("strafe1");
                    strafen.add(strafe1);
                    flags.add(vnt.getInt("flag1"));
                    String strafe2 = vnt.getString("strafe2");
                    strafen.add(strafe2);
                    flags.add(vnt.getInt("flag2"));

                    //eventsList.append(strafe1 + "\n\n" + strafe2 + "\n");
                }

                ArrayAdapter oweAd = new ArrayAdapter(ChoosePunishmentActivity.this, android.R.layout.simple_list_item_1,strafen){

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent){
                        // Get the Item from ListView
                        View view = super.getView(position, convertView, parent);

                        // Initialize a TextView for ListView each Item
                        TextView tv = (TextView) view.findViewById(android.R.id.text1);

                        // Set the text color of TextView (ListView Item)
                        tv.setTextColor(Color.WHITE);

                        // Generate ListView Item using TextView
                        return view;
                    }
                };

                dynamic.setAdapter(oweAd);

            }else System.out.println("Empty");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(eventsList.getText().toString() != "") {

                    String punishment = null;
                    try {
                        punishment = URLEncoder.encode(eventsList.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    new HttpPoster().execute("game", id, punishment, ""+pos, "addPunishment");


                    new HttpPoster().execute("game", id, "start");
                    Intent intent = new Intent(ChoosePunishmentActivity.this, GameActivity.class);
                    startActivity(intent);
                }

            }
        });

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChoosePunishmentActivity.this, EnterOwnPunishmentActivity.class);
                startActivity(intent);
            }
        });

        dynamic.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = dynamic.getItemAtPosition(position);
                eventsList.setText("");
                eventsList.append(strafen.get(position));
                pos = position;
            }
        });
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(ChoosePunishmentActivity.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(ChoosePunishmentActivity.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
