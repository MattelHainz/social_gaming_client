package com.app.socialgamingandroidtutorial;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser fbUser= mAuth.getCurrentUser();

        //If there is no User logged in
        if(fbUser==null) {
            //the login page will be loaded
            final EditText emailText = (EditText) findViewById(R.id.emailText);
            final EditText passwordText = (EditText) findViewById(R.id.passwordText);
            final Button signIn = findViewById(R.id.signinButton);
            final Button register = findViewById(R.id.register_Button);

            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signIn(emailText.getText().toString(), passwordText.getText().toString());
                }
            });

            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(intent);
                }
            });
        }
        //If there is a User logged in
        else{
            //The map activity will be loadeed
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            Log.d(TAG,"signedinUser: "+ fbUser);
            Toast.makeText(LoginActivity.this, "Signed in" + fbUser, Toast.LENGTH_SHORT).show();
        }
    }

    //A method for signing in a user to firebase
    private void signIn(String email,String password){
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "singedinUserwithEmail:success");
                    //passiert nur wen user existiert
                    //if(emailText.getText()!= null){
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    //}
                }else{
                    Log.d(TAG,"signedinUserwithEmail:no success");
                    Toast.makeText(LoginActivity.this, "Sign in failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
