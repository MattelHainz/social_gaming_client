package com.app.socialgamingandroidtutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;

public class AddFriend extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        final TextView myID = (TextView) findViewById(R.id.myID);
        final EditText editFireBaseID = findViewById(R.id.editFireBaseID);
        final Button add = findViewById(R.id.add_button);
        final Button back = findViewById(R.id.back_button);


        myID.setText(""+FirebaseAuth.getInstance().getUid());
        /*
        editFireBaseID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFireBaseID.setText("");
            }
        });
        */
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isnull = isNull(editFireBaseID.getText().toString());
                info(isnull,0);
                if(isnull)
                    return;
                boolean ownID = isIDownID(editFireBaseID.getText().toString());
                info(ownID,1);
                if(ownID)
                    return;
                boolean inDB = isIDinDB(editFireBaseID.getText().toString());
                info(inDB,2);
                if(inDB)
                    return;
                boolean isAFriend = isAFriend(editFireBaseID.getText().toString());
                info(isAFriend,3);
                if(isAFriend)
                    return;
                boolean addFriend = addFriend(editFireBaseID.getText().toString());
                info(addFriend,4);
                editFireBaseID.setText("");

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddFriend.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void info(boolean check, int checkID)
    {
        TextView info = (TextView) findViewById(R.id.resultinfo);
        EditText editFireBaseID = findViewById(R.id.editFireBaseID);

        switch(checkID)
        {
            case 0:
                if(check)
                {
                    info.setText("PLEASE ENTER AN ID.");
                    info.setTextColor(Color.rgb(255, 69, 0));
                }
                break;
            case 1:
                if(check) {
                    info.setText("YOU CAN NOT ADD YOURSELF.");
                    info.setTextColor(Color.rgb(255, 69, 0));
                }
                break;
            case 2:
                if(check) {
                    info.setText("YOU CAN NOT ADD AN UNKNOWN ID.");
                    info.setTextColor(Color.rgb(255, 69, 0));
                }
                break;
            case 3:
                if(check) {
                    info.setText("YOU CAN NOT ADD A FRIEND TWICE.");
                    info.setTextColor(Color.rgb(255,165,0));
                }
                break;
            case 4:
                info.setText("SUCCESSFULLY ADDED THE FRIEND:\n"+editFireBaseID.getText().toString());
                info.setTextColor(Color.rgb(0,255,127));
                break;

        }

    }
    // CASE 0: ID is NULL => PLEASE ENTER A ID
    private boolean isNull(String givenID){
        if(givenID.compareTo("")==0)
            return true;
        else
            return false;
    }

    // CASE 1: ID is OWN ID => YOU CAN NOT ADD YOURSELF
    private boolean isIDownID(String givenID){

        //System.out.println(""+givenID+","+FirebaseAuth.getInstance().getUid().toString());

        if(givenID.compareTo(FirebaseAuth.getInstance().getUid().toString())==0)
            return true;
        else
            return false;
    }

    // CASE 2: ID is NOT in DB => YOU CAN NOT ADD AN UNKNOWN ID
    private boolean isIDinDB(String givenID)
    {
        HttpGetter get = new HttpGetter();
        get.execute("friend",
                givenID,
                "checkID");
        try{
            String getResult = get.get();

            if (getResult.equals("{}"))
                return true;
            else
                return false;

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        //optimistic default
        return true;


    }
    // CASE 3: ID IS ALREADY A FRIEND => YOU CAN NOT ADD A FRIEND TWICE
    private boolean isAFriend(String givenID) {
        HttpGetter get = new HttpGetter();
        get.execute("friend",
                FirebaseAuth.getInstance().getUid(),
                givenID,
                "checkIfFriend");
        try{
            String getResult = get.get();

            if (getResult.equals("{}"))
                return false;
            else
                return true;

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        //pessimistic default
        return false;

    }
    // CASE 4: PREVIOUS CHECKS ARE FALSE => SUCCESSFULLY ADDED A FRIEND
    private boolean addFriend(String givenID){

        HttpPoster post = new HttpPoster();
        post.execute("friend",
                FirebaseAuth.getInstance().getUid(),
                givenID,
                "addFriend");
        return true;
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(AddFriend.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(AddFriend.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
