package com.app.socialgamingandroidtutorial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

public class Debts extends Activity {

    ListView owList;
    ListView getList;

    ArrayList<String> oweItems=new ArrayList<String>();
    ArrayList<String> getItems=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debts);

        final Button back = findViewById(R.id.backDebtsMain);

        HttpGetter get = new HttpGetter();
        get.execute("punish",""+ FirebaseAuth.getInstance().getUid(),"getGetPunishList");

        try{
            String getResult = get.get();

            if (!getResult.equals("{ }")) {
                JSONObject json = new JSONObject(getResult);
                JSONArray jsonDebts = json.getJSONArray("AllGetDebts");

                for (int idx = 0; idx < jsonDebts.length(); idx++) {
                    JSONObject jsonPunish = jsonDebts
                            .getJSONObject(idx);

                    String userName = jsonPunish
                            .getString("user");
                    String punishment = jsonPunish
                            .getString("punishment");

                    getItems.add("From: "+userName+" Punishment: "+punishment);

                }
            } else System.out.println("Empty");


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        HttpGetter get2 = new HttpGetter();
        get2.execute("punish",""+ FirebaseAuth.getInstance().getUid(),"getOwerPunishList");

        try{
            String getResult = get2.get();

            if (!getResult.equals("{ }")) {
                JSONObject json = new JSONObject(getResult);
                JSONArray jsonDebts = json.getJSONArray("AllOweDebts");

                for (int idx = 0; idx < jsonDebts.length(); idx++) {
                    JSONObject jsonPunish = jsonDebts
                            .getJSONObject(idx);

                    String userName = jsonPunish
                            .getString("user");
                    String punishment = jsonPunish
                            .getString("punishment");

                    oweItems.add("User: "+userName+" Punishment: "+punishment);

                }
            } else System.out.println("Empty");


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        owList = findViewById(R.id.oweList);
        ArrayAdapter oweAd = new ArrayAdapter(Debts.this, android.R.layout.simple_list_item_1,oweItems);
        owList.setAdapter(oweAd);

        getList = findViewById(R.id.getList);
        ArrayAdapter getAd = new ArrayAdapter( Debts.this, android.R.layout.simple_list_item_1,getItems);
        getList.setAdapter(getAd);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Debts.this, MainActivity.class);
                startActivity(intent);
            }
        });

        getList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = getList.getItemAtPosition(position);
                Toast.makeText(getBaseContext(),position+": "+o.toString(),Toast.LENGTH_SHORT).show();

                String item = getList.getAdapter().getItem(position).toString();

                String[] sp = item.split("From:|Punishment:");

                String punishment = null;
                try {
                    punishment = URLEncoder.encode(sp[2], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                new HttpPoster().execute("punish",""+FirebaseAuth.getInstance().getUid(),""+sp[1].trim(),""+punishment,"pullPunishFromList");

                finish();
                startActivity(getIntent());

            }
        });


    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(Debts.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(Debts.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
