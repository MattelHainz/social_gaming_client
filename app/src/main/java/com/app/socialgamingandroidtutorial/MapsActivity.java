package com.app.socialgamingandroidtutorial;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.content.Context;

import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;

import com.app.socialgamingandroidtutorial.Util.HttpGetter;
import com.app.socialgamingandroidtutorial.Util.HttpPoster;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    int MY_RESULT_ACCESS_FINE_LOCATION;

    private LocationManager locationManager;
    private LocationListener locationListener;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        new HttpPoster().execute("init",
                "init");

        // Add a marker in Sydney and move the camera
        //LatLng garching = new LatLng(48.262617, 11.668276);
        //mMap.addMarker(new MarkerOptions().position(garching).title("TUM Informatik"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(garching));

        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_RESULT_ACCESS_FINE_LOCATION);
        } else {
            mMap.setMyLocationEnabled(true);

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                @SuppressLint("MissingPermission")
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                //Log.d("LocationTest",""+lastKnownLocation.getLatitude());
                //Log.d("LocationTest",""+lastKnownLocation.getLongitude());
                drawLocationsOfFriends();
                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location user) {
                        //LatLng location = new LatLng(user.getLatitude(), user.getLongitude());
                        mMap.clear();
                        drawLocationsOfFriends();

                    }
                });
            }
        }
    }

    public void drawLocationsOfFriends(){
        Log.d("Test","Http GET");
        HttpGetter get = new HttpGetter();
        get.execute("position",
                "getPostionOfFriends");
        try{
            String getResult = get.get();

            if (!getResult.equals("{ }")) {
                JSONObject json = new JSONObject(getResult);
                JSONArray jsonUsrs = json.getJSONArray("AllFriends");

                for (int idx = 0; idx < jsonUsrs.length(); idx++) {
                    JSONObject jsonLocation = jsonUsrs
                            .getJSONObject(idx);

                    String userName = jsonLocation
                            .getString("user");
                    String loc1 = jsonLocation
                            .getString("loc1");
                    String loc12= jsonLocation
                            .getString("loc12");

                    Log.d("Test",userName+","+loc1+","+loc12);

                  /*
                    String loc2 = jsonLocation
                            .getString("loc2");
                    String loc22 = jsonLocation
                            .getString("loc22");
                    String loc3 = jsonLocation
                            .getString("loc3");
                    String loc32 = jsonLocation
                            .getString("loc32");
                    */
                    /*
                    if(!loc3.equals(" ")) {
                        System.out.println(loc3 + " " + loc32);

                        LatLng location3 = new LatLng(Double.parseDouble(loc3), Double.parseDouble(loc32));
                        mMap.addMarker(new MarkerOptions().position(location3).title("Third Last Position of " + userName));
                    }
                    if(!loc2.equals(" ")) {
                        System.out.println(loc2 + " " + loc22);

                        LatLng location2 = new LatLng(Double.parseDouble(loc2), Double.parseDouble(loc22));
                        mMap.addMarker(new MarkerOptions().position(location2).title("Second Last Position of " + userName));
                    }
                    */
                    if(!loc1.equals(" ")) {
                        System.out.println(loc1 + " " + loc12);

                        LatLng location1 = new LatLng(Double.parseDouble(loc1), Double.parseDouble(loc12));
                        mMap.addMarker(new MarkerOptions().position(location1).title("Last Position of " + userName));
                        //mMap.moveCamera(CameraUpdateFactory.newLatLng(location1));
                    }
                }
            } else System.out.println("Empty");


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Taken from https://stackoverflow.com/questions/3141996/android-how-to-override-the-back-button-so-it-doesnt-finish-my-activity
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        AlertDialog alertDialog = new AlertDialog.Builder(MapsActivity.this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Doing this will log you out");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = new Intent(MapsActivity.this,LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}


